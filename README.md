<h1 align="center">BRUTALIST BLOG BUILDER</h1>
<h2 align="center">Minimalistic website builder</h2>

<p>This application lets you build a simple webpage using robust elements in html and responsive css. No javascripts, cookies or other tracking components.</p>

<p>The project directory is organised as follows:<br>
<br>
[projectname]<br>
  + _media<br />
    + [imagename]11.jpg (1024x1024 pixels)<br />
    + [imagename]169.jpg (1024x576 pixels)<br />
    + [imagename]512.jpg (512x512 pixels)<br />
  + pages<br />
    + [pagename].xml<br />
    + ...<br />
  + posts<br />
    + [postname].xml<br />
    + ...<br />
<br />
• main.xml<br />
• stylemaster.css (stylesheet that will be copied to public_html as [domainname]/style.css)<br />

See /example_files for xml file structure.</p>

##LICENSE
GPL v3.
