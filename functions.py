from datetime import date;
from datetime import time;
from datetime import datetime;
import re

def GetStrippedLabel(label):
    return re.sub(r"[^a-zA-Z0-9]","",label);
def GetDateTime(XML):
    date = datetime.strptime(XML.find('date').text,"%Y-%m-%d");
    hour = int(XML.find('date_hour').text);
    minute = int(XML.find('date_minute').text);
    date = date.replace(hour=hour,minute=minute);
    #print('GetDateTime() ',date);
    return date;
