
import argparse
import sys;
import numpy;
import xml.etree.ElementTree as ET;
import os;
import shutil;
from shutil import copyfile
from datetime import date;
from datetime import time;
from datetime import datetime;
import locale
#import re
from operator import itemgetter

import functions

# 1. Parse arguments
parser = argparse.ArgumentParser(description='Brutalist Web Builder')
parser.add_argument('--localProjectPath', dest='localProjectPath', help='The local project source path.', default='', action='store');
parser.add_argument('--localPublishPath', dest='localPublishPath', help='The local publish path.', default='', action='store');
args = parser.parse_args();

if args.localProjectPath == '':
    localProjectPath = input('Local Project path not set. Please type the path (or drag the folder) of your local project source directory and press enter. ').replace(' ','');
else:
    localProjectPath = args.localProjectPath;

if args.localPublishPath == '':
    localPublishPath = input('Local publish path not set. Please type the path (or drag the folder) of your local publish directory and press enter. ').replace(' ','');
else:
    localPublishPath = args.localPublishPath;

print('--localProjectPath: ',localProjectPath);
print('--localPublishPath: ',localPublishPath);

# 2. Global variables
mediaFiles = ['favicon.ico','icon-menu.png','icon-close.png','icon-git.png','icon-pixelfed.png','icon-mastodon.png','start11.jpg','start169.jpg','start512.jpg','start21.jpg','og_default.jpg'];#assumes there is a .ico file 64x64 pixels in the projects "_media"-directory.
all_labels = [];
all_labels_frequencies = [];
all_labels_first_urls = [];

# load main xml-file [project_directory]/main.xml
print('loading main.xml');
mainXMLdoc = ET.parse(localProjectPath+'/main.xml')
mainXML = mainXMLdoc.getroot()

#vars from xml
domain = mainXML.find('domain').text;

print('domain: ',domain);
#lang = mainXML.find('language').text.join(mainXML.find('language').text.split());
lang = mainXML.find('language').text;
print('lang: ',lang);
#locale.setlocale(locale.LC_TIME,lang)

domain = domain;

print('domain ',domain);

# 3. Build RSS-feed (initional channel-part)
rss = '<?xml version="1.0" encoding="UTF-8"?>\n<rss version="2.0" xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:wfw="http://wellformedweb.org/CommentAPI/" ';
rss += 'xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:atom="http://www.w3.org/2005/Atom" xmlns:sy="http://purl.org/rss/1.0/modules/syndication/" xmlns:slash="http://purl.org/rss/1.0/modules/slash/">';
rss += '\n\t<channel>\n\t\t<title>'+mainXML.find('title').text+'</title>\n\t\t<atom:link href="'+domain+'/feed/rss.xml" rel="self" type="application/rss+xml" />\n\t\t<link>'+domain+'</link>';
rss += '\n\t\t<description>'+mainXML.find('description').text+'</description>\n\t\t<lastBuildDate>'+datetime.today().strftime('%a, %d %b %Y %H:%M:%S +0000')+'</lastBuildDate>\n\t\t<language>'+mainXML.find('language').text+'</language>';
rss += '\n\t\t<sy:updatePeriod>hourly</sy:updatePeriod>\n\t\t<sy:updateFrequency>1</sy:updateFrequency>\n\t\t<generator>python_custom_code</generator>';
rss += '\n\t\t<image>\n\t\t\t<url>'+domain+'/_media/favicon.ico</url>\n\t\t\t<title>'+mainXML.find('title').text+'</title>\n\t\t\t<link>'+domain+'</link>\n\t\t\t<width>64</width>\n\t\t\t<height>64</height>\n\t\t</image>';

# 4. Defining Functions
def BuildSummaryPage(filePath,directoryName,stylePath,menuIndex,posts,pages,filter_label=""):
    content = '';
    title =  '';

    print('buildsumpage: ',filePath);

    if filter_label != '':
        content += '<h5>#'+filter_label+'</h5>';
        title = filter_label+' - '+mainXML.find('title').text;
    else:
        title = mainXML.find('title').text;

    content += '<br>&nbsp;';

    for i in range(0,len(posts)):
        XMLdoc = ET.parse(localProjectPath+'/posts/'+posts[i][1]);
        XML = XMLdoc.getroot();
        filenameArray = posts[i][1].split(".");
        labels = [];
        if XML.find('labels').text is not None:
            labels = XML.find('labels').text.split(",");

        if filter_label == "" or filter_label in labels:
            objectURL = '../../'+filenameArray[0]+'/index.html';
            content += '\n\t\t\t\t<div class="summaryObject">';
            content += '<a href="'+objectURL+'" style="text-decoration:none;"><img class="main" src="../_media/'+XML.find('image').text+'169.jpg" width="300" height="300" style="position:relative;top:24pt;left:1.5%;width:97%;">';
            content += '<h5 style="position:relative;top:10pt;">'+XML.find('title').text+'</h5></a>';
            content += '<p style="position:relative;top:-20pt;">'+XML.find('excerpt').text+' <span class="readMore"><a href="'+objectURL+'">'+mainXML.find('readMore').text+'</a></span></p></div>\n';

    for i in range(0,len(pages)):
        XMLdoc = ET.parse(localProjectPath+'/pages/'+pages[i])
        XML = XMLdoc.getroot()
        filenameArray = pages[i].split(".")

        labels = [];
        if XML.find('labels').text is not None:
            labels = XML.find('labels').text.split(",");

        if filter_label == "" or filter_label in labels:
            objectURL = '../../'+filenameArray[0]+'/index.html';
            content += '\n\t\t\t\t<div class="summaryObject">';
            content += '<a href="'+objectURL+'" style="text-decoration:none;"><img class="main" src="'+domain+'/_media/'+XML.find('image').text+'169.jpg" width="300" height="300" style="position:relative;top:24pt;left:1.5%;width:97%;">';
            content += '<h5 style="position:relative;top:10pt;">'+XML.find('title').text+'</h5></a>';
            content += '<p style="position:relative;top:-20pt;">'+XML.find('excerpt').text+' <span class="readMore"><a href="'+objectURL+'">'+mainXML.find('readMore').text+'</a></span></p></div>\n';

    BuildHTML(filePath,directoryName,stylePath,title,mainXML.find('description').text,menuIndex,content,[]);

def BuildHTML(filePath,directoryName,stylePath,title,description,menuIndex,content,labels):

    BuildHTMLPage(filePath,directoryName,stylePath,title,description,menuIndex,content,labels,False);
    BuildHTMLPage(filePath,directoryName,stylePath,title,description,menuIndex,content,labels,True);
	
def BuildHTMLPage(filePath,directoryName,stylePath,title,description,menuIndex,content,labels,showMenu):

    html_string = '<!DOCTYPE html>\n<html lang="sv-SE">\n\t<head>';
    html_string += '\n\t\t<meta charset="UTF-8">';
    html_string += '\n\t\t<meta http-equiv="content-type" content="text/html">';

    html_string += '\n\t\t<meta NAME="ROBOTS" CONTENT="INDEX, FOLLOW">';

    html_string += '\n\t\t<meta name="viewport" content="width=device-width, initial-scale=1.0">';
    html_string += '\n\t\t<meta name="description" content="'+description+'">';
    html_string += '\n\t\t<title>'+title+'</title>';
    html_string += '\n\t\t<link rel="stylesheet" type="text/css" href="'+stylePath+'">';
    html_string += '<link rel="shortcut icon" href="'+domain+'/_media/favicon.ico">';

    html_string += '\n\t\t<meta property="og:type" content="website" />';
    html_string += '\n\t\t<meta property="og:url" content="'+domain+'" />';
    html_string += '\n\t\t<meta property="og:title" content="'+title+'" />';
    html_string += '\n\t\t<meta property="og:image" content="'+domain+'/_media/og_default.jpg" />';

    #html_string += '\n\t\t<link rel="alternate" type="application/rss+xml" title="';
    #html_string += mainXML.find('title').text;
    #html_string += '" href="'+domain+'/feed/rss.xml">';
    html_string += '\n\t</head>';
    html_string += '\n\t<body>';
    html_string += '\n\t\t<div id="header">';

    html_string += '\n\t\t\t<nav class="navigation">';

    #menu
    if showMenu == False:
        html_string += '<div class="menuSymbol"><a href="..'+directoryName+'/indexmenu.html"><img src="https://arvidmarklund.se/_media/icon-menu.png" width="25" height="25"></a></div></nav></div>';
    else:
        html_string += '<div class="menuCloseSymbol"><a href="..'+directoryName+'/index.html"><img src="https://arvidmarklund.se/_media/icon-close.png" width="25" height="25"></a></div>';
        html_string += '\n\t\t\t\t<ul class="menu">';

        i = 0;
        for menuitem in mainXML.findall('menuitem'):
            html_string += '\n\t\t\t\t\t<li><a ';
            if i == menuIndex:
                html_string += 'class="activelink" ';
            html_string += 'href="'

            html_string += '..'+menuitem.get('directory')+'/index.html';

            html_string += '">';
            html_string += menuitem.get('name')
            html_string += '</a></li>';
            i += 1;

        html_string += '\n\t\t\t\t</ul>\n\t\t\t</nav>';
        html_string += '\n\t\t</div>';

    html_string += '\n\t\t<div id="content">'+content;

    labelHTML = '\n\t\t\t<div id="labels">';
    for l in range(0,len(labels)):
        stripped_label = functions.GetStrippedLabel(labels[l]);
        labelHTML += '\n\t\t\t\t<span class="label">';

        match_index = all_labels.index(labels[l]);
        if all_labels_frequencies[match_index] > 1:
            labelHTML += '<a href="../labels/'+stripped_label+'/index.html">#'+labels[l]+'</a>';
        else:#only exist on current page, so no link.
            labelHTML += '#'+labels[l];

        labelHTML += '</span>';

    labelHTML += '\n\t\t\t</div>';

    html_string += labelHTML+'\n\t\t</div>';

    all_labels_html = '';
    for i in range(0,len(all_labels)):
        match_index = all_labels.index(all_labels[i]);
        if all_labels_frequencies[match_index] > 1:
            linkURL = '../labels/'+functions.GetStrippedLabel(all_labels[i])+'/index.html';
        else:
            linkURL = all_labels_first_urls[i];

        baseFontSize = 5;
        frequencyBonus = 2 * all_labels_frequencies[i];
        all_labels_html += '<div class="footerLabel" style="font-size:'+str(baseFontSize+frequencyBonus)+'pt;"><a href="'+linkURL+'">#'+all_labels[i]+'</a></div>';

    #footer
    html_string += '\n\t\t<div id="footer">';
    html_string += '\n\t\t\t<div id="footer1">'+mainXML.find('bio').text+'<br>&nbsp;<br></div>';
    html_string += '\n\t\t\t<div id="footer2">'+all_labels_html+'</div>';
    html_string += '\n\t\t</div>';

    html_string += '\n\t</body>\n</html>';

    print('BuildHTML() path: ',filePath);
    #save file
    if showMenu == True:
        print(html_string, file=open(filePath+'/indexmenu.html', 'w'));
    else:
        print(html_string, file=open(filePath+'/index.html', 'w'));


# 5. Loading Content
#load pages
print('loading pages');
pages = [n for n in os.listdir(localProjectPath+'/pages') if not n.startswith(".")]
#load posts
print('loading posts');
posts = [n for n in os.listdir(localProjectPath+'/posts') if not n.startswith(".")];

live_posts = [];
for i in range(0,len(posts)):
    XMLdoc = ET.parse(localProjectPath+'/posts/'+posts[i]);
    XML = XMLdoc.getroot();
    if XML.find('status').text != 'draft':
        live_posts.append(posts[i]);

posts = live_posts;

live_pages = [];
for i in range(0,len(pages)):
    print("checking if live, page: ",pages[i])
    XMLdoc = ET.parse(localProjectPath+'/pages/'+pages[i]);
    XML = XMLdoc.getroot();
    if XML.find('status').text != 'draft':
        live_pages.append(pages[i]);

pages = live_pages;

#scan labels
#get alla labels in pages
print('scanning labels');
for i in range(0,len(pages)):
    XMLdoc = ET.parse(localProjectPath+'/pages/'+pages[i]);
    XML = XMLdoc.getroot();
    filenameArray = pages[i].split(".")
    labels = [];
    if XML.find('labels').text is not None:
        labels = XML.find('labels').text.split(",");
        for j in range(0,len(labels)):
            if labels[j] not in all_labels:
                all_labels.append(labels[j]);
                all_labels_frequencies.append(1);
                all_labels_first_urls.append(domain+'/'+filenameArray[0]+'/index.html');
            else:
                match_index = all_labels.index(labels[j]);#find index of this object in all_labels.
                all_labels_frequencies[match_index] = all_labels_frequencies[match_index] + 1;#all_labels_frequencies change increase by one.

#get alla labels in posts
for i in range(0,len(posts)):
    print('loading file: ',posts[i]);
    XMLdoc = ET.parse(localProjectPath+'/posts/'+posts[i]);
    XML = XMLdoc.getroot();
    filenameArray = posts[i].split(".");
    labels = [];
    if XML.find('labels').text is not None:
        labels = XML.find('labels').text.split(",");
        for j in range(0,len(labels)):
            if labels[j] not in all_labels:
                all_labels.append(labels[j]);
                all_labels_frequencies.append(1);
                all_labels_first_urls.append(domain+'/'+filenameArray[0]+'/index.html');
            else:
                match_index = all_labels.index(labels[j]);#find index of this object in all_labels.
                all_labels_frequencies[match_index] = all_labels_frequencies[match_index] + 1;#all_labels_frequencies change increase by one.

#clear temp public_html
if os.path.isdir(localPublishPath+'/public_html'):
    shutil.rmtree(localPublishPath+'/public_html');

# 6. Build Content
print('building pages');
for i in range(0,len(pages)):
    XMLdoc = ET.parse(localProjectPath+'/pages/'+pages[i])
    XML = XMLdoc.getroot()
    filenameArray = pages[i].split(".")
    labels = [];
    if XML.find('labels').text is not None:
        labels = XML.find('labels').text.split(",");

    image=''
    if XML.find('image').text is not None:
        mediaFiles.append(XML.find('image').text+'11.jpg')
        mediaFiles.append(XML.find('image').text+'21.jpg')
        mediaFiles.append(XML.find('image').text+'169.jpg')
        mediaFiles.append(XML.find('image').text+'512.jpg')
        image = '<img class="main" src="'+domain+'/_media/'+XML.find('image').text+'21.jpg" width="300" height="150">';

    #check for <img etc media references in pages content and include to mediaFiles for copying. Func that be used for both posts and pages.
    path_dir = localPublishPath+'/public_html/'+filenameArray[0];
    path_file = path_dir+'/index.html';

    publish_date = functions.GetDateTime(XML);
    if not os.path.isdir(path_dir):
        os.makedirs(path_dir)


    htmlTitle='';
    
    if XML.find('title').text != None:
        htmlTitle = str(XML.find('title').text)+' - '+str(mainXML.find('title').text);
        headline = '<h3 style="position:relative;top:4pt;">'+str(XML.find('title').text)+'</h3>';
    else:
        htmlTitle = str(mainXML.find('title').text);
        headline = '<h3 style="position:relative;top:4pt;"> </h3>';

    BuildHTML(path_dir,'/'+filenameArray[0],'../style.css',htmlTitle,XML.find('excerpt').text,i,headline+image+XML.find('text').text,labels);

#sort posts according to date
sorted_posts_array = [];

for i in range(0,len(posts)):
    if not posts[i].startswith('.'):
        XMLdoc = ET.parse(localProjectPath+'/posts/'+posts[i])
        XML = XMLdoc.getroot()
        post_array = [];
        post_array.append(XML.find('date').text);
        post_array.append(posts[i]);
        sorted_posts_array.append(post_array);

sorted_posts_array = sorted(sorted_posts_array, key=itemgetter(0),reverse=True);

#build posts
print('building posts');
for i in range(0,len(sorted_posts_array)):
    XMLdoc = ET.parse(localProjectPath+'/posts/'+sorted_posts_array[i][1]);
    XML = XMLdoc.getroot();
    filenameArray = sorted_posts_array[i][1].split(".");
    labels = [];
    if XML.find('labels').text is not None:
        labels = XML.find('labels').text.split(",");

	#rss feed (items-part)
    thedate = datetime.strptime(XML.find('date').text,'%Y-%m-%d');
    enclosureURL = domain+'/_media/'+XML.find('image').text+'512.jpg';
    enclosureLength = str(os.path.getsize(localProjectPath+'/_media/'+XML.find('image').text+'512.jpg'));
    enclosureType = 'image/jpg';

    rss += '\n\t\t<item>';
    rss += '\n\t\t\t<title><![CDATA['+XML.find('title').text+']]></title>';
    rss += '\n\t\t\t<link>'+domain+'/'+filenameArray[0]+'/index.html</link>';
    rss += '\n\t\t\t<pubDate>'+thedate.strftime("%a, %d %b %Y %H:%M:%S +0000")+'</pubDate>';
    rss += '\n\t\t\t<dc:creator>'+XML.find('creator').text+'</dc:creator>';
    rss += '\n\t\t\t<description>'+XML.find('excerpt').text+'</description>';
    rss += '\n\t\t\t<enclosure url="'+enclosureURL+'" length="'+enclosureLength+'" type="'+enclosureType+'" />';

    for j in range(0,len(labels)):
        rss += '\n\t\t\t<category><![CDATA['+labels[j]+']]></category>';

    rss += '\n\t\t</item>';

    mediaFiles.append(XML.find('image').text+'11.jpg')
    mediaFiles.append(XML.find('image').text+'169.jpg')
    mediaFiles.append(XML.find('image').text+'512.jpg')

    #check for <img etc media references in post content and include to mediaFiles for copying.

    path_dir = localPublishPath+'/public_html/'+filenameArray[0];
    path_file = path_dir+'/index.html';
    publish_date = functions.GetDateTime(XML);

    if not os.path.isdir(path_dir):
        os.makedirs(path_dir)

    dateString = '\n\t\t\t<p style="font-family:sans-serif;text-align:right;font-size:10pt;color:#666666;text-transform:uppercase;">'+thedate.strftime("%a, %d %b %Y")+'</p>';
    headline = '\n\t\t\t<h3 style="position:relative;top:4pt;">'+XML.find('title').text+'</h3>';
    image = '\n\t\t\t<img class="main" src="'+domain+'/_media/'+XML.find('image').text+'21.jpg" width="300" height="150">\n\t\t\t';

    BuildHTML(path_dir,'/'+filenameArray[0],'../style.css',XML.find('title').text+' - '+mainXML.find('title').text,XML.find('excerpt').text,-1,headline+image+XML.find('text').text+dateString,labels);

# 7. Finish RSS-file. (ending and write to file)
rss += '\n\t</channel>\n</rss>'

#write rss only if it has posts
if len(posts) > 0:
    os.makedirs(localPublishPath+'/public_html/feed')
    print(rss, file=open(localPublishPath+'/public_html/feed/rss.xml', 'w'))
    #print('rss: '+rss);

# 8. Move Files
# style_sheet (style_master.css)
copyfile(localProjectPath+'/style_master.css',localPublishPath+'/public_html/style.css');

# mediafiles
os.makedirs(localPublishPath+'/public_html/_media');
for i in range(0,len(mediaFiles)):
    copyfile(localProjectPath+'/_media/'+mediaFiles[i],localPublishPath+'/public_html/_media/'+mediaFiles[i]);

# 9. Make home and summary pages
# make summary page for all unique labels (with freq greater than one)
print('building summary pages for frequent labels');
for i in range(0,len(all_labels)):
    if all_labels_frequencies[i] > 1:
        label = all_labels[i];
        stripped_label = functions.GetStrippedLabel(label);
        path_dir = localPublishPath+'/public_html/labels/'+stripped_label;
        path_file = path_dir+'/index.html';

        if not os.path.isdir(path_dir):
            os.makedirs(path_dir);

        BuildSummaryPage(localPublishPath+'/public_html/labels/'+stripped_label,'/labels/'+stripped_label,'../../style.css',-1,sorted_posts_array,pages,all_labels[i]);

if len(posts) == 0:
    print('copy start to index.html och indexmenu.html')

else:
    print('building home summary for posts');
    #3 make startpage with latest posts (all labels)
    BuildSummaryPage(localPublishPath+'/public_html/','','./style.css',0,sorted_posts_array,[]);
